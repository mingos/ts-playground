var Jasmine = require("jasmine");
var SpecReporter = require("jasmine-spec-reporter");

var noop = function() {};

var jrunner = new Jasmine();
jrunner.configureDefaultReporter({print: noop});
jasmine.getEnv().addReporter(new SpecReporter({
	colors: process.argv.indexOf("--noColours") === -1,
	displayStacktrace: process.argv.indexOf("--noStackTrace") === -1 ? "all" : "none"
}));
jrunner.loadConfigFile();
jrunner.execute();
