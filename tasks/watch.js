var gulp = require("gulp");
var runSequence = require("run-sequence");

gulp.task("watch", function() {
	gulp.watch("src/**/*.ts", function() {
		runSequence("ts:src", "test");
	}).on("error", function() {});
	gulp.watch("spec/**/*.ts", function() {
		runSequence("ts:spec", "test");
	}).on("error", function() {});
});
