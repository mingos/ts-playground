var gulp = require("gulp");
var jasmine = require("gulp-jasmine");
var SpecReporter = require("jasmine-spec-reporter");

gulp.task("test", function() {
	gulp.src("spec/**/*.spec.js")
		.pipe(jasmine({
			config: require("../spec/support/jasmine.json"),
			reporter: new SpecReporter({
				colors: true,
				displayStackTrace: true
			})
		}));
});
