var gulp = require("gulp");
var ts = require("gulp-typescript");
var merge = require("merge-stream");

var tsconfig = require("../tsconfig.json").compilerOptions;
var devConfig = {
	module: tsconfig.module,
	target: tsconfig.target,
	emitDecoratorMetadata: tsconfig.emitDecoratorMetadata,
	experimentalDecorators: tsconfig.experimentalDecorators,
	noImplicitAny: tsconfig.noImplicitAny,
	moduleResolution: tsconfig.moduleResolution,
	declaration: tsconfig.declaration
};

gulp.task("ts:src", function() {
	return gulp.src([tsconfig.rootDir + "/**/*.ts", "typings/index.d.ts"])
		.pipe(ts(devConfig))
		.pipe(gulp.dest("src"));
});

gulp.task("ts:spec", function() {
	return gulp.src(["spec/**/*.ts", "typings/index.d.ts"])
		.pipe(ts(devConfig))
		.pipe(gulp.dest("spec"));
});

gulp.task("ts:dev", ["ts:src", "ts:spec"]);

gulp.task("ts:dist", function() {
	var result = gulp.src([tsconfig.rootDir + "/**/*.ts", "typings/index.d.ts"])
		.pipe(ts(tsconfig));

	var stream1 = result.dts
		.pipe(gulp.dest(tsconfig.outDir));
	var stream2 = result.js
		.pipe(gulp.dest(tsconfig.outDir));

	return merge(stream1, stream2);
});
