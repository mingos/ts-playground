TypeScript Playground
=====================

This is a playground project for TypeScript. Use as a base for TypeScript-based projects.

## Installation

```
npm install
typings install
```

## Run tests

```
npm test
```

or

```
gulp test
```

or

```
jasmine
```

## Watch for changes

```
npm run watch
```

or

```
gulp watch
```

## Build output

```
npm run build
```

or

```
gulp
```
